package bookshop.models;

public class BookShop {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BookShop(String name) {
        this.name = name;
    }


}
