package bookshop.controllers;


import bookshop.models.BookShop;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BookController {

     @RequestMapping("/application")
     public BookShop bookShop() {
         return new BookShop("Book Shop");

     }
}
